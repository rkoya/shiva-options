import { HoverStyle } from './HoverStyle';
import { StyleDeclaration } from './StyleDeclaration';

export interface HoverStyleDeclaration extends StyleDeclaration {
	hover?: HoverStyle
}