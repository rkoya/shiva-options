﻿import { ContainerConfig } from './ContainerConfig';

export interface AnchorConfig extends ContainerConfig {
    href?: any;
}